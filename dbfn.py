import sqlite3 as db
def getParams(db_string):
    #print db_string
    conn = db.connect(db_string)
    cur = conn.cursor()
    query  = "SELECT id, name, value from params order by id"

    #print("query is: ", query )
    cur.execute(query)
    rows = cur.fetchall()
    params = {}
    if  len(rows) > 0:
        for row in rows:
            params[row[0].rstrip()] = [row[1].rstrip(),row[2].rstrip()]
    conn.close()
    return params 

def setParam(db_string,id,val):
    conn = db.connect(db_string)
    cur = conn.cursor()
    query  = "UPDATE params set value =? where id = ?"
    
    #print("query is: ", query )
    cur.execute(query,(val,id))
    conn.commit()
    conn.close()

def addParam(db_string, id, val,desc):
    conn = db.connect(db_string)
    cur = conn.cursor()
    q1 = "delete from params where id =?"
    q2 = "insert into params values( ? ,?,?)"
    cur.execute(q1,(id,))
    cur.execute(q2, (val, id,desc))
    conn.commit()
    conn.close()
    
def getMeterList(db_string,usbPort):
    conn = db.connect(db_string)
    cur = conn.cursor()
    query  = "SELECT meterid from meter_lists where port = ?"
    cur.execute(query,(usbPort,))

    rows = cur.fetchall()
    meterlist = []
    if  len(rows) > 0:
        for row in rows:
            meterlist.append(row[0])
    conn.close()
    return meterlist

def getAllMeterList(db_string):
    conn = db.connect(db_string)
    cur = conn.cursor()
    query  = "SELECT port,meterid from meter_lists"
    cur.execute(query)

    rows = cur.fetchall()
    return rows
    
    
def addMeter(db_string,usbPort,meterid):
    conn = db.connect(db_string)
    cur = conn.cursor()
    query  = "insert into meter_lists values(?,?)"
    cur.execute(query,(usbPort,meterid))
    
    conn.commit()
    conn.close()

def deleteMeter(db_string,usbPort,meterid):
    conn = db.connect(db_string)
    cur = conn.cursor()
    query  = "delete from meter_lists where port = ? and meterid = ?"
    cur.execute(query,(usbPort,meterid))
    conn.commit()
    conn.close()
    
def saveMsg(db_string,msg_list):
    conn = db.connect(db_string)
    cur = conn.cursor()
    query  = "insert into messages (topic,msg) VALUES (?,?)"
    print query,len(msg_list)
    cur.executemany(query,msg_list)
    conn.commit()
    conn.close()
    
def deleteMsg(db_string,id_list):
    #print "delete called"
    conn = db.connect(db_string)
    cur = conn.cursor()
    query  = "delete from messages where id =?"
    #print query,len(id_list)
    #cur.execute(query,len(id_list))
    cur.executemany(query,id_list)
    conn.commit()
    conn.close()
    
def getMsg(db_string):
    conn = db.connect(db_string)
    cur = conn.cursor()
    query  = "select id,topic,msg from messages order by id limit 2000"
    cur.execute(query)
    rows = cur.fetchall()
    #print query,len(rows)
    #id = None
    #topic = None
    #msg = None
    #if  len(rows) > 0:
    #    id = rows[0][0]
    #    topic =rows[0][1]
    #    msg = rows[0][2]
    conn.close()
    return (rows) 

def getMsgCount(db_string):
    conn = db.connect(db_string)
    cur = conn.cursor()
    query  = "select count(*) from messages"
    cur.execute(query)
    rows = cur.fetchall()
    #print query,len(rows)
    #id = None
    #topic = None
    #msg = None
    #if  len(rows) > 0:
    #    id = rows[0][0]
    #    topic =rows[0][1]
    #    msg = rows[0][2]
    conn.close()
    return (rows[0][0])
    #conn.close()
