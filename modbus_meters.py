import argparse
import os
from os.path import join
import time 
from pymodbus.client.sync import ModbusSerialClient as ModbusClient
import serial
import pymodbus
import struct
import multiprocessing 
#import queue
#from Queue import Queue
from readThread import readThread 
from writeMqttThread import writeMqttThread
from monitorThread import monitorThread
import dbfn

def find_tty_usb(idVendor, idProduct):
    """find_tty_usb('067b', '2302') -> '/dev/ttyUSB0'"""
    # Note: if searching for a lot of pairs, it would be much faster to search
    # for the enitre lot at once instead of going over all the usb devices
    # each time.
    portList=[]
    for dnbase in os.listdir('/sys/bus/usb/devices'):
        dn = join('/sys/bus/usb/devices', dnbase)
        if not os.path.exists(join(dn, 'idVendor')):
            continue
        idv = open(join(dn, 'idVendor')).read().strip()
        if idv != idVendor:
            continue
        idp = open(join(dn, 'idProduct')).read().strip()
        if idp != idProduct:
            continue
        for subdir in os.listdir(dn):
            if subdir.startswith(dnbase + ':'):
                for subsubdir in os.listdir(join(dn, subdir)):
                    if subsubdir.startswith('ttyUSB'):
                        portList.append(join('/dev', subsubdir))
            #print join('/dev', subsubdir)
    return portList

def runMainProcesses(db_string,params):
    portList = find_tty_usb("0403", "6001")
    
    print portList
    q = multiprocessing.Queue()
    monitor_q = multiprocessing.Queue()


    p_monitor = monitorThread("Monitor",monitor_q,db_string)
    p_monitor.start()

    dbfn.setParam(db_string, 'exit', 'N')
    last_check = time.time()

    threadList = {}
    for port in portList:
        print "Creating ", port
        threadList[port] = readThread(port,q,monitor_q,db_string)
        #time.sleep(1)
    for p in threadList:
        print "Starting ", p
        threadList[p].start()
        #time.sleep(1)

    mqttProcess = writeMqttThread("writeMqtt",q,monitor_q,db_string)
    mqttProcess.start()
    
    valid =False
    validResponse = 'exit'
    while (valid == False):
        time.sleep(20)
        for p in threadList:
            if not threadList[p].is_alive():
                print "restarting process for ", p
                threadList[p] = readThread(p,q,monitor_q,db_string)
                threadList[p].start()
        if not mqttProcess.is_alive():
            print "restarting mqtt process "
            mqttProcess = writeMqttThread("writeMqtt",q,monitor_q,db_string)
            mqttProcess.start()

        if not p_monitor.is_alive():
            print "restarting monitor process "
            p_monitor = monitorThread("Monitor",monitor_q,db_string)
            p_monitor.start()

        if last_check + 60 > time.time():
            params = dbfn.getParams(db_string)
            if params['exit'][1] == 'Y':
                quit = True
            last_check = time.time()

        # print "Type command (exit) to exit."
        # userInput = raw_input(">>> ")
        # if (userInput == validResponse):
        #    valid = True
    #client.loop_stop(force=False)
    for u in threadList:
        print "Stopping ", u
        threadList[u].stop()
        #time.sleep(1)
    
    for p in threadList:
        print "Joining ", p
        threadList[p].join()
        #time.sleep(1)

    mqttProcess.stop()
    mqttProcess.join()
    p_monitor.stop()
    p_monitor.join()
    dbfn.setParam(db_string, 'exit', 'X')

    
def main(args):
    #print(args)
    
    db_name = "modbus_meters.sqlite"
    g_params = dbfn.getParams(db_name)

    parser = argparse.ArgumentParser(description='Smart Meter data selection')
    group1 = parser.add_mutually_exclusive_group(required=False)
    group1.add_argument('-l', action="store_true", dest="list", help="List parameters")
    group1.add_argument('-m', action="store_true", dest="listmeters", help="List meters")
    group1.add_argument('-c', action="store_true", dest="clearmeters", help="Clear meters list")
    group1.add_argument('-q', action="store_true", dest="queuecount", help="give que message count")
    group1.add_argument('-r', action="store_true", dest="run", help="Run the program")
    group1.add_argument('-s', action="store", dest="set", help="where SET is <parameter>=<value>")
    group1.add_argument('-a', action="store", dest="add", help="where ADD is <parameter>,<value>,<description>")

    arguments = parser.parse_args()
    #print arguments
    
    if arguments.run:
        print "running !!!"
        runMainProcesses(db_name,g_params)

    if arguments.queuecount:
        queCount = dbfn.getMsgCount(g_params['msgdb'][1])
        print  "Queue Count:",  queCount

    if arguments.listmeters:
        meterList = dbfn.getAllMeterList(db_name)
        for m in meterList:
            print  m

    if arguments.clearmeters:
        meterList = dbfn.getAllMeterList(db_name)
        for m in meterList:
            #print  m
            dbfn.deleteMeter(db_name, m[0], m[1])


    if arguments.list:
        for key in g_params:
            print  '%-15s%-30s%-30s' % (key ,  ":" + g_params[key][1],"  (" + g_params[key][0] +")") 

    if arguments.add is not None:
        param_val = arguments.add.split(",")
        if len(param_val) != 3:
            print "need <parameter>,<value>,<description>"
        else:
            dbfn.addParam(db_name,param_val[0],param_val[1],param_val[2])
        g_params = dbfn.getParams(db_name)
        for key in g_params:
            print  '%-15s%-30s%-30s' % (key, ":" + g_params[key][1], "  (" + g_params[key][0] + ")")

    if arguments.set is not None:
        param_val = arguments.set.split("=")
        if len(param_val) != 2:
            print "need <parameter>=<value> "
        else:
            dbfn.setParam(db_name,param_val[0],param_val[1])
            g_params = dbfn.getParams(db_name)
            for key in g_params:
                print  '%-15s%-30s%-30s' % (key ,  ":" + g_params[key][1],"  (" + g_params[key][0] +")") 
        
if __name__ == '__main__':
    import sys
    main(sys.argv[1:]) 