import time 
from pymodbus.client.sync import ModbusSerialClient as ModbusClient
import serial
import pymodbus
import sys
import socket
import struct
import multiprocessing
import time
#import queue
#from Queue import Queue
import dbfn
try:
    import fcntl
except:
    None

class readThread(multiprocessing.Process):
    def __init__(self,usbPort,qname,monitor_q,dbName):
        super(readThread, self).__init__()
        self.usbPort=usbPort
        self.dbname=dbName
        self.params = dbfn.getParams(dbName)
        if self.params['topic'][1].split("/")[1] == 'sch':
            self.meter_type = "S"
        if self.params['topic'][1].split("/")[1] == 'rish':
            self.meter_type = "R"
        self.ip = None
        self.qname=qname
        self.monitor_q = monitor_q
        self.checkMeter=0
        self.readMeter=0
        self.readCount=0 
        self.meterList = dbfn.getMeterList(self.dbname,self.usbPort)
        self.failureCount = [0 for i in range(len(self.meterList))]
        self.rate = 0
        self.readInfo = 1
        if self.meter_type == 'S':
            self.rate = 19200
            self.register= 3900
            self.bank1 = 3900
            self.bank2 = -1
            self.query_length = 66
            self.reply_message = 'ReadRegisterResponse (66)'
        elif self.meter_type == 'R':
            self.rate = 9600
            self.register = 20
            self.bank1 = 0
            self.bank2 = 34
            self.query_length = 40
            self.reply_message = 'ReadRegisterResponse (40)'


        #self.mbClient = ModbusClient(retries=2, method='rtu',baudrate=19200, stopbits=1, parity='N', bytesize=8, timeout=1, port=self.usbPort)
        self.mbClient = ModbusClient(retries=2, method='rtu',baudrate=self.rate, stopbits=1, parity='N', bytesize=8, timeout=1, port=self.usbPort)

        if self.meter_type == 'S':
            self.data_function = self.mbClient.read_holding_registers
        elif self.meter_type == 'R':
            self.data_function = self.mbClient.read_input_registers

        self.exit = multiprocessing.Event()
        
    def checkForNewMeter(self):
        counter = 0 
        while 1:
            counter = counter  + 1
            self.checkMeter =  self.checkMeter + 1
            if self.checkMeter > 255: 
                self.checkMeter = 1
            if self.checkMeter not in self.meterList:
                break
            if counter >= 10:
                return
        print self.usbPort, "checking for Meter id:", self.checkMeter, 


        #print 'calling mbClient'
        binary_data = self.mbClient.read_holding_registers(self.register, 2, unit=self.checkMeter)
        if binary_data is not None:
            if(str(binary_data)=="ReadRegisterResponse (2)"):
                if self.meter_type == 'R':
                    # in case of rish meter additional check can be performed
                    s = (binary_data.registers[0] << 16) + binary_data.registers[1]
                    id = struct.unpack("<f", struct.pack("<I", s))[0]
                    print "Respose ", id
                    if id != self.checkMeter:
                        return

                self.meterList.append(self.checkMeter)
                self.failureCount.append(0)
                dbfn.addMeter(self.dbname,self.usbPort,self.checkMeter)
                print 'found meter:' , self.meterList
            else:
                print "Not Found"
        else:
            print "None"
       
    def getNextMeterData(self):
        if len(self.meterList) > 0:
            self.readMeter = self.readMeter + 1
            if self.readMeter >= len(self.meterList):
                self.readMeter = 0
            #print "Merer" , self.meterList[self.readMeter]
            reg1 = None
            reg2 = None
            response = None
            ctr = 0
            while response is None: 
                ctr = ctr + 1
                if ctr >= 3:
                    break
                response = self.data_function(self.bank1 , self.query_length , unit=self.meterList[self.readMeter])
            #if response is not None:
            if(str(response) == self.reply_message):
                reg1 = response.registers
                    #print "reg1", reg1
            if self.bank2 > 0:
                response = None
                ctr = 0
                while response is None:
                    ctr = ctr + 1
                    if ctr >= 3:
                        break
                    response = self.data_function(34 , 40, unit=self.meterList[self.readMeter])
                #if response is not None:
                if(str(response)=="ReadRegisterResponse (40)"):
                    reg2 = response.registers

            if reg1 is not None:
                self.failureCount[self.readMeter] = 0

            if reg1 is None:
                self.failureCount[self.readMeter] = self.failureCount[self.readMeter] + 1
                print "Failed", self.meterList[self.readMeter]
            # when complete message is received
            ts = float(time.time())

            if self.meter_type == 'S':
                if reg1 is not None:
                    binary_data = reg1
                    str_data = []
                    for i in range(0, (len(binary_data) - 1), 2):
                        # different for meters
                        s = ((binary_data[i + 1] << 16) + binary_data[i])
                        str_data.append("{0:.6f}".format(struct.unpack("<f", struct.pack("<I", s))[0]))
                    # data = ['data',self.meterList[self.readMeter],float(time.time())] + str_data
                    topic = 'data/' + self.params['topic'][1] + '/' + str(self.meterList[self.readMeter])
                    payload = [str(x) for x in str_data]
                    data = [topic,ts,",".join(payload)]
                    #print "putting S data to queue"
                    self.qname.put(data)

            if self.meter_type == 'R':
                if reg1 is not None and reg2 is not None:
                    binary_data = reg1 + reg2
                    str_data = []
                    for i in range(0, (len(binary_data) - 1), 2):
                        s = ((binary_data[i] << 16) + binary_data[i + 1])
                        str_data.append(str(struct.unpack("<f", struct.pack("<I", s))[0]))

                    #data = ['data', str(self.meterList[self.readMeter]), str(time.time())] + str_data
                    topic = 'data/' + self.params['topic'][1] + '/' + str(self.meterList[self.readMeter])
                    payload = [str(x) for x in str_data]
                    data = [topic,ts,",".join(payload)]
                    #print "putting R data to queue"
                    self.qname.put(data)

            if self.params['meter_list'][1] == 'D':
                if self.failureCount[self.readMeter] > 100:
                    dbfn.deleteMeter(self.dbname, self.usbPort, self.meterList[self.readMeter])
                    del self.meterList[self.readMeter]
                    del self.failureCount[self.readMeter]
                    self.sendMeterList()

                        #print "Putting", self.meterList[self.readMeter]
            #if self.failureCount[self.readMeter] > 10000:
            #    db.deleteMeter(self.dbname,self.usbPort,self.meterList[self.readMeter])
            #    del self.meterList[self.readMeter]
            #    del self.failureCount[self.readMeter]
        else:
            print "No Meter to read"
            time.sleep(1)


    def getNextMeterInfo(self):
        if len(self.meterList) > 0:
            self.readInfo = self.readInfo + 1
            if self.readInfo >= len(self.meterList):
                self.readInfo = 0
            #print "Merer" , self.meterList[self.readInfo]
            reg1 = None
            reg2 = None
            response = None
            ctr = 0
            while response is None:
                ctr = ctr + 1
                if ctr >= 3:
                    break
                response = self.mbClient.read_holding_registers(0 , 40, unit=self.meterList[self.readInfo])
            #if response is not None:
            if(str(response)=="ReadRegisterResponse (40)"):
                reg1 = response.registers
                    #print "reg1", reg1
            response = None
            ctr = 0
            while response is None:
                ctr = ctr + 1
                if ctr >= 3:
                    break
                response = self.mbClient.read_holding_registers(40 , 40, unit=self.meterList[self.readInfo])
            #if response is not None:
            if(str(response)=="ReadRegisterResponse (40)"):
                reg2 = response.registers
                #print "Read successful", self.usbPort,self.meterList[self.readInfo]
                    #print "reg2", reg2
                    #temp=convert_to_str((binary_data.registers[i + 1] << 16) + binary_data.registers[i])
                    #data = [self.usbPort,self.meterList[self.readInfo],str(str(time.time())) ,response.registers]
                    #print data
            #    self.qname.put(data)
            if reg1 is not None and reg2 is not None:
                self.failureCount[self.readInfo] = 0
            if reg1 is None and reg2 is None:
                self.failureCount[self.readInfo] = self.failureCount[self.readInfo] + 1
            #    reg1 = [1,2,3,4,5]
            #    reg2 = [11,12,13,14,self.failureCount[self.readInfo]]
            if reg1 is not None and reg2 is not None:
                binary_data = reg1 + reg2
                str_data = []
                for i in range(0, (len(binary_data) - 1), 2):
                    s = ((binary_data[i] << 16) + binary_data[i + 1])
                    str_data.append(str(struct.unpack("<f", struct.pack("<I", s))[0]))

                ts = float(time.time())
                # data = ['config', str(self.meterList[self.readMeter]), str(time.time())] + str_data
                topic = 'config/' + self.params['topic'][1] + '/' + str(self.meterList[self.readMeter])
                payload = [str(x) for x in str_data]
                data = [topic, ts, ",".join(payload)]

                #print "putting config to queue"
                self.qname.put(data)

            if self.failureCount[self.readInfo] > 100:
                dbfn.deleteMeter(self.dbname,self.usbPort,self.meterList[self.readInfo])
                del self.meterList[self.readInfo]
                del self.failureCount[self.readInfo]
                self.sendMeterList()
            #else:
            #    print "Data Missing"
            #    reg1 = [1,2,3,4,5]
            #    reg2 = [11,12,13,14,15]
            #time.sleep(2)
        else:
            print "No Meter to read"
            #time.sleep(1)

    def sendMeterList(self):
        # data = ['info', self.usbPort, str(time.time())] + [str(x) for x in self.meterList]
        ts = float(time.time())
        # data = ['config', str(self.meterList[self.readMeter]), str(time.time())] + str_data
        topic = 'info/' + self.params['topic'][1] + '/' + self.ip + '_' + self.usbPort.split("/")[-1]

        payload = [str(x) for x in self.meterList]
        data = [topic, ts, ",".join(payload)]
        self.qname.put(data)

    def stop(self):
        self.exit.set()
        
    def run(self):
        # check once for each meter
        last_meter_check = 0
        if len(self.meterList) == 0:
            for x in range(256):
                # print 'Scanning', self.usbPort, x
                self.checkForNewMeter()

        while not self.exit.is_set():
            if self.ip is None:
                try:
                    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                    if sys.platform == 'win32':
                        hostname = socket.gethostname()
                        self.ip = socket.gethostbyname(hostname)
                    else:
                        self.ip = socket.inet_ntoa(fcntl.ioctl(s.fileno(), 0x8915, struct.pack('256s', "eth0"))[20:24])

                except Exception, err:
                    print Exception, err, "Error Finding IP"

            try:
                debug = 0
                if last_meter_check + 60 < float(time.time()):
                    # self.monitor_q.put("MeterReadCount:" + str(self.readCount))
                    # self.monitor_q.put("MeterList:" + "|".join([str(x) for x in self.meterList]))
                    last_meter_check = float(time.time())
                #time.sleep(0.02)
                #print "in the loop", self.meterList, self.readMeter
                self.getNextMeterData()                
                self.readCount = self.readCount + 1
                if (self.readCount % 10000) == 999:
                    debug = 1
                    self.sendMeterList()
                if self.meter_type == 'R' and (self.readCount % 1000) == 15:
                    debug = 2
                    self.getNextMeterInfo()
                if self.params['meter_list'][1] == 'D' and last_meter_check + 60 < float(time.time()) :
                    debug = 3
                    self.checkForNewMeter()
                    last_meter_check = float(time.time())
            except Exception,err:
                print Exception,err
                print "Error in run debug = ", debug
        print self.usbPort, "Finished"
