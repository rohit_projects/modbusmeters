import time 
import paho.mqtt.client as mqtt
import multiprocessing 
import dbfn
import Queue
import struct
import socket
import errno
import sys
import paho.mqtt.publish as pub

try:
    import fcntl
except:
    None

class writeMqttThread(multiprocessing.Process):
    def __init__(self,threadID,qname,monitor_q,dbName):
        super(writeMqttThread, self).__init__()
        self.threadID=threadID
        self.qname=qname
        self.connected = False
        # self.initialConnect = False
        self.dbName = dbName
        self.monitor_q = monitor_q
        self.params = dbfn.getParams(dbName)
        self.exit = multiprocessing.Event()
        self.sent_count = 0
        self.saved_count = 0
        self.last_debug = 0
        self.last_msg = {}
        self.last_msg_no = {}
        self.discard_count = 0
        self.recv_count = 0
        self.failureCount = 0
        self.mqttClient = None
        self.ip = None

    def stop(self):
        self.exit.set()

    def get_next_queue_message(self):
        """
        :return: (topic,msg)
        if no message returns (None,None)
        if duplicate message returns (0,None)
        """
        try:
            data = self.qname.get(timeout=0.1)
            #print data
            topic = data[0]
            msg_data = data[2]
            self.recv_count += 1
            if topic in self.last_msg:
                if msg_data == self.last_msg[topic]:
                    #print "Discarding duplicate" , data[1]
                    self.discard_count += 1
                    return (0,None) # change topic to "Duplicate"
            else:
                self.last_msg_no[topic] = 0

            self.last_msg[topic] = msg_data
            self.last_msg_no[topic] += 1
            #print "Message No", self.last_msg_no[data[1]]
            #print "New Message"
            # print "Topic", topic ,data[1]
            # print data
            return (data[0], str(self.last_msg_no[topic]) + ',' + str(data[1]) + ',' + data[2])
        except Queue.Empty:
            return (None,None)

    def send_message(self, msg):
        try:
            # print msg, self.params['server_ip'][1],self.params['server_port'][1]
            # print self.ip
            pub.single(msg[0], payload=msg[1], qos=1, hostname=self.params['server_ip'][1], will=None,
                       port=int(self.params['server_port'][1]), client_id='pi@' + str(self.ip), auth=None,
                       retain=False, keepalive=60, tls=None, protocol=mqtt.MQTTv311, transport="tcp")
            self.connected = True
            return 0
        except Exception, err:
            self.connected = False
            self.monitor_q.put("Stat:" + 'E02')
            print time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time())), Exception, 'writeMqttThread pub.single error', err
            return 1

        return 2


    def send_ip(self):
        if self.ip is not None:
            data = ['info/' + self.params['topic'][1] + '/' + str(self.ip) ,str(self.ip)]
            self.send_message(data)


    def run(self):
        print "Starting MQTT ",self.threadID
        msg_to_send = []
        last_saved = float(time.time())
        last_error = ""
        param_refresh = float(time.time())
        try:
            while not self.exit.is_set() or self.qname.qsize() > 0 or len(msg_to_send) > 0:

                if self.ip is None:
                    try:
                        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                        if sys.platform == 'win32':
                            hostname = socket.gethostname()
                            self.ip = socket.gethostbyname(hostname)
                        else:
                            self.ip = socket.inet_ntoa(fcntl.ioctl(s.fileno(), 0x8915, struct.pack('256s', "eth0"))[20:24])
                    except Exception, err:
                        print Exception, '*', err, "writeMqttThread Error in getting IP"

                if self.last_debug + 60 < time.time():
                    self.last_debug = time.time()
                    self.monitor_q.put("Sent:" + str(self.sent_count))
                    self.monitor_q.put("Saved:" + str(self.saved_count))
                    self.monitor_q.put("Discard:" + str(self.discard_count))
                    self.monitor_q.put("Recv:" + str(self.recv_count))
                    self.recv_count = 0
                    self.sent_count = 0
                    self.discard_count = 0
                stat =[]
                for m in self.last_msg_no:
                    stat.append(str(m) + "-" + str(self.last_msg_no))
                self.monitor_q.put("Meters:" + str(self.saved_count))

                for x in range(1000) :
                    (topic,msg) = self.get_next_queue_message()
                    if topic is None:
                        break
                    if topic != 0:
                        #print topic
                        msg_to_send.append((topic,msg))

                # print "message count", len(msg_to_send), self.connected,self.ip
                # Refresh the parameter from the database
                if float(time.time() > param_refresh + 60):
                    # print param_refresh , float(time.time())
                    param_refresh = float(time.time())
                    self.params = dbfn.getParams(self.dbName)

                if self.params['online'][1] != 'Y':
                    self.connected = False

                if not self.connected:
                    self.send_ip()
                    # saving messages if it is required
                    if float(time.time()) > last_saved  + 60 or self.exit.is_set():
                        # print time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time())),"saving messages", len(msg_to_send)
                        dbfn.saveMsg(self.params['msgdb'][1],msg_to_send)
                        self.saved_count += len(msg_to_send)
                        msg_to_send = []
                        last_saved = float(time.time())


                while self.connected:
                    # Recovery state, send the stored messages first
                    stored_msg_list = dbfn.getMsg(self.params['msgdb'][1])
                    if len(stored_msg_list) > 0:
                        # print time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time())),"Send saved messages", len(stored_msg_list)
                        delete_ids = []
                        for msg in stored_msg_list:
                            # offline messsages have id also
                            if self.send_message(msg[1:]):
                                break
                            delete_ids.append((msg[0],))
                            self.sent_count += 1
                            self.monitor_q.put("Stat:" + 'N02')

                        # Delete the messages from storage that are already sent
                        if len(delete_ids) > 0:
                            # print time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time())),"deleting saved messages", len(delete_ids)
                            dbfn.deleteMsg(self.params['msgdb'][1],delete_ids)
                        # if disconnected while sending
                        if not self.connected:
                            break
                    else:
                        # go to online state
                        break

                while (self.connected and len(msg_to_send) > 0 ):
                    # online state
                    msg = msg_to_send[0]
                    # print time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time())), "Sending live ", msg[0]
                    if self.send_message(msg):
                        break

                    # print time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time())),"success"
                    self.monitor_q.put("Stat:" + 'N02')
                    msg_to_send.remove(msg)
                    self.saved_count = 0
                    self.sent_count += 1

        except Exception,err:
            self.connected = False
            self.monitor_q.put("Stat:" + 'E04')
            if last_error != str(err):
                print time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time())),Exception,'writeMqttThread *',err
                last_error = str(err)

                # print "Error in run"
        print "Writer Stopped"
        if self.mqttClient is not None:
            self.mqttClient.disconnect()