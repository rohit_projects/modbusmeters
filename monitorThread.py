import socket
import sys
import multiprocessing
import dbfn
import Queue
import time
import struct
import socket

try:
    import fcntl
except:
    None


class monitorThread(multiprocessing.Process):
    def __init__(self, threadID, qname, dbName):
        super(monitorThread, self).__init__()
        self.qname = qname
        self.params = dbfn.getParams(dbName)
        self.threadID = threadID
        self.exit = multiprocessing.Event()
        self.recv_count = 0
        self.send_count = 0
        self.sent_time = 0
        self.status = {}
        self.send_list = self.params['send_list'][1].split("|")

        for field in self.send_list:
            self.status[field] = "#"
        print self.status

    def stop(self):
        self.exit.set()

    def run(self):
        time.sleep(60)
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        if sys.platform == 'win32':
            hostname = socket.gethostname()
            self_ip = socket.gethostbyname(hostname)
        else:
            self_ip = socket.inet_ntoa(fcntl.ioctl(s.fileno(), 0x8915, struct.pack('256s', "eth0"))[20:24])
        self.self_ip = self_ip
        self.status['Stat'] = "X01"

        #self.status['MeterList'] = ""
        #self.status['MeterReadCount'] = 0
        #self.status['BridgeMsgNo'] = ""
        # Create a TCP/IP socket

        # Bind the socket to the port

        server_address = (self.params['debug_ip'][1], int(self.params['debug_port'][1]))

        while not self.exit.is_set():
            try:
                msg = self.qname.get(timeout=0.1)
                rec = msg.split(":")
                self.status[rec[0]] = rec[1]
            except Queue.Empty:
                None
            new_time = time.time()

            if new_time > self.sent_time + 60:
                try:
                    msg = "#" + "Rpi_" + self.self_ip
                    for field in range(len(self.send_list)):
                        if self.send_list[field] not in self.status:
                            continue
                        msg += "," + str(self.status[self.send_list[field]])

                    # print "sending message"
                    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                    sent = sock.sendto(msg + ";", server_address)
                    self.sent_time = int(new_time)
                    sock.close()
                    self.status['Stat'] = "X01"
                except Exception, err:
                    # print Exception, err
                    # print "Error in Monitor run"
                    None
